// This component handles the App template used on every page.
import React, { PropTypes } from 'react';
import MainHeader from './common/header/MainHeader';
import MainSideBar from './common/sidebar/MainSideBar';
import MainFooter from './common/footer/MainFooter';
import { connect } from 'react-redux';

class App extends React.Component {
    render() {
        return (
            <div className="wrapper">
                <header className="main-header">
                    <MainHeader />
                </header>
                <aside className="main-sidebar">
                    <MainSideBar />
                </aside>
                {this.props.children}
                <footer className="main-footer">
                  <MainFooter />
                </footer>
                
            </div>
        );
    }
}

App.propTypes = {
    children: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {

    };
}

export default connect(mapStateToProps)(App);
